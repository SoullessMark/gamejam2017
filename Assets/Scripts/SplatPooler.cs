﻿using UnityEngine;
using System.Collections;

public class SplatPooler : MonoBehaviour {
    public GameObject Splat;
    public GameObject SplatParticle;
    public GameObject StaticSplat;

    public int MaxSplats;
    public int MaxSplatParticles;
    public int MaxStaticSplats;

    public GameObject[] Splats;
    public GameObject[] SplatParticles;
    public GameObject[] StaticSplats;
	// Use this for initialization
	void Start () {
        Splats = new GameObject[MaxSplats];
        for(int i = 0; i < MaxSplats; i++)
        {
            Splats[i] = (GameObject) Instantiate(Splat, gameObject.transform.position, gameObject.transform.rotation);
            Splats[i].SetActive(false);
        }
        SplatParticles = new GameObject[MaxSplatParticles];
        for(int i = 0; i < MaxSplatParticles; i++)
        {
            SplatParticles[i] = (GameObject) Instantiate(SplatParticle, gameObject.transform.position, gameObject.transform.rotation);
            SplatParticles[i].SetActive(false);
        }
        StaticSplats = new GameObject[MaxStaticSplats];
        for(int i = 0; i < MaxStaticSplats; i++)
        {
            StaticSplats[i] = (GameObject)Instantiate(StaticSplat, gameObject.transform.position, gameObject.transform.rotation);
            StaticSplats[i].SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
