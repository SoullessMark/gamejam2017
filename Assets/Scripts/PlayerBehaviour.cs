﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {
    private bool running = true;
    private bool HasHit = false;
    private float magnitude;
    private float duration;
    private Vector3 Target;
    public GameObject Hand;
    private Vector3 _Hand;
    public Animator _HandAnim;
	// Use this for initialization
	void Start () {
        _Hand = Hand.gameObject.transform.position;
       
        Cursor.visible = false;
        StartCoroutine(PlayerMovement());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator PlayerMovement()
    {
        while (running)
        {
            CheckInput();
            MovePlayer();
            MoveHand();
            yield return new WaitForSeconds(.015f);
        }
    }
    public void CheckInput()
    {
        if (Input.GetMouseButtonDown(0) && !HasHit)
        {
            HasHit = true;
            _HandAnim.SetInteger("State", 1);
            magnitude = .35f;
            duration = .25f;
          
            StartCoroutine(AttackReset(GameLogicUtil.GameLogicUtil.HandAttackOne));
        }
        if (Input.GetMouseButtonDown(1) && !HasHit)
        {
            HasHit = true;
            _HandAnim.SetInteger("State", 2);
            magnitude = .15f;
            duration = .15f;
            
            StartCoroutine(AttackReset(GameLogicUtil.GameLogicUtil.HandAttackTwo));
        }
    }
    IEnumerator AttackReset(AnimationClip StartedPlaying)
    {
        yield return new WaitForSeconds(StartedPlaying.length);
        _HandAnim.SetInteger("State", 0);
        HasHit = false;
    }
    public void MovePlayer()
    {
        Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Target = new Vector3(Target.x, Target.y, 0);
        float distance = GameLogicUtil.GameLogicUtil.GetDistance(gameObject.transform.position, Target);
        if(distance > .01f)
        {
           gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, Target, Time.deltaTime * 65);
        }
    }
    public void MoveHand()
    {
        Target = gameObject.transform.position;
        float distance = GameLogicUtil.GameLogicUtil.GetDistance(_Hand, Target);
        if(distance > .01f && !HasHit)
        {
           _Hand = Vector3.MoveTowards(_Hand , Target, Time.deltaTime * 10);
            Hand.gameObject.transform.position = _Hand;
        }
    }
    public void Shake()
    {
        StartCoroutine(GameLogicUtil.GameLogicUtil.Shake(magnitude, duration));
    }
}
