﻿using UnityEngine;
using System.Collections;
namespace GameLogicUtil
{
    
    public class GameLogicUtil : MonoBehaviour
    {
        public SplatPooler _Pooler;
        public Animator _HandAnim;
        public AnimationClip _HandIdle;
        public AnimationClip _HandAttackOne;
        public AnimationClip _HandAttackTwo;
        public AnimationClip _Splat;
        public static AnimationClip HandIdle;
        public static AnimationClip HandAttackOne;
        public static AnimationClip HandAttackTwo;
        public static AnimationClip Splat;
        public static Animator HandAnim;
        
        public static SplatPooler Pooler;
        // Use this for initialization
        void Start()
        {
            Pooler = _Pooler;
            HandIdle = _HandIdle;
            HandAttackOne = _HandAttackOne;
            HandAttackTwo = _HandAttackTwo;
            HandAnim = _HandAnim;
            Splat = _Splat;
        }

        // Update is called once per frame
        void Update()
        {

        }
        public static float GetDistance(Vector3 init, Vector3 target)
        {
            float distance = Mathf.Sqrt((Mathf.Pow(target.x - init.x, 2)) + (Mathf.Pow(target.y - init.y, 2)));
            return distance;
            
        }
        public static IEnumerator Shake(float magnitude, float duration)
        {

            float elapsed = 0.0f;

            Vector3 originalCamPos = Camera.main.transform.position;

            while (elapsed < duration)
            {

                elapsed += Time.deltaTime;

                float percentComplete = elapsed / duration;
                float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

                // map value to [-1, 1]
                float x = Random.value * 2.0f - 1.0f;
                float y = Random.value * 2.0f - 1.0f;
                x *= magnitude * damper;
                y *= magnitude * damper;

                Camera.main.transform.position = new Vector3(x, y, originalCamPos.z);

                yield return null;
            }

            Camera.main.transform.position = originalCamPos;
        }

    }
    
}
