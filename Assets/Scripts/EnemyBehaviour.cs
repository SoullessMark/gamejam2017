﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {
    public Sprite[] States;
    public bool active = true;
    public bool Stunned = false;
    public bool WaveHit = false;
    public bool CenterHit = false;
    public bool Dead = false;
    private bool HasDied = false;
    public float StunTime;
    private bool TimerStarted = false;
    private Vector3 Center;
	// Use this for initialization
	void Start () {
        gameObject.GetComponent<SpriteRenderer>().sprite = States[0];
        Center = GameObject.FindWithTag("Center").gameObject.transform.position;
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator DeathCheck()
    {
        while (active)
        {
            if (Dead)
            {
                HasDied = true;
                Dead = false;
                SplatPooler TempPool = GameLogicUtil.GameLogicUtil.Pooler;
                
                    for (int i = 0; i < TempPool.MaxSplatParticles; i++)
                    {
                        if (TempPool.SplatParticles[i].activeInHierarchy == false)
                        {
                            TempPool.SplatParticles[i].transform.position = gameObject.transform.position;
                            TempPool.SplatParticles[i].SetActive(true);
                            Vector3 Target = TempPool.SplatParticles[i].GetComponent<SplatParticle>().Target = gameObject.transform.position;
                            TempPool.SplatParticles[i].GetComponent<SplatParticle>().Target = new Vector3(Target.x + Random.Range(-.45f, .45f), Target.y + Random.Range(-.45f, .45f));
                            TempPool.SplatParticles[i].GetComponent<SplatParticle>().BlowingUp = true;
                            TempPool.SplatParticles[i].GetComponent<SplatParticle>()._Splat();
                            break;
                            
                        }
                    }
                    for (int k = 0; k < TempPool.MaxSplatParticles; k++)
                  {
                    if (TempPool.SplatParticles[k].activeInHierarchy == false)
                    {
                        TempPool.SplatParticles[k].transform.position = gameObject.transform.position;
                        TempPool.SplatParticles[k].SetActive(true);
                        Vector3 Target = TempPool.SplatParticles[k].GetComponent<SplatParticle>().Target = gameObject.transform.position;
                        TempPool.SplatParticles[k].GetComponent<SplatParticle>().Target = new Vector3(Target.x + Random.Range(-.45f, .45f), Target.y + Random.Range(-.45f, .45f));
                        TempPool.SplatParticles[k].GetComponent<SplatParticle>().BlowingUp = true;
                        TempPool.SplatParticles[k].GetComponent<SplatParticle>()._Splat();
                        break;

                    }
                 }

                for (int j = 0; j < TempPool.MaxSplats; j++)
                {
                    if (TempPool.Splats[j].activeInHierarchy == false)
                    {
                        TempPool.Splats[j].transform.position = gameObject.transform.position;
                        TempPool.Splats[j].SetActive(true);
                        break;
                    }
                }
            
                
                gameObject.SetActive(false);
            }
            yield return new WaitForSeconds(.1f);
        }
    }
    IEnumerator DistanceCheck()
    {
        while (active)
        {
            float distance = GameLogicUtil.GameLogicUtil.GetDistance(gameObject.transform.position, Center);
            if(distance < 2 && gameObject.transform.position.y > .01f)
            {
               Color TempColor = GameObject.FindWithTag("Center").gameObject.GetComponent<SpriteRenderer>().color;
                GameObject.FindWithTag("Center").gameObject.GetComponent<SpriteRenderer>().color = new Color(TempColor.r, TempColor.g, TempColor.b, .75f);
            }
            yield return new WaitForSeconds(.25f);
        }
    }
    IEnumerator EnemyMovement()
    {
        while (active)
        {
            CheckSprite();
            MoveEnemy();
            yield return new WaitForSeconds(.03f);
        }
    }
    public void MoveEnemy()
    {
        float distance = GameLogicUtil.GameLogicUtil.GetDistance(gameObject.transform.position, Center);
        if(distance > .1f && !Stunned)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, Center, Time.deltaTime / .75f);
        }
        if (WaveHit && Stunned)
        {
            StunTime = 10.75f;
            ForceBack(-15.75f);
        }
        if (CenterHit && Stunned)
        {
            StunTime = 5.15f;
            ForceBack(-7.5f);
        }
        if (Stunned && !TimerStarted)
        {
            TimerStarted = true;
            StopCoroutine(EffectStun(0.0f));
            StopCoroutine(TimeStun(0.0f));
            StartCoroutine(EffectStun(StunTime));
        }
    }
    public void ForceBack(float force)
    {
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, Center, Time.deltaTime * force);
    }
    public void CheckSprite()
    {
        Animator Temp = gameObject.GetComponent<Animator>();
        if(gameObject.transform.position.x > .75f)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = States[0];
            if(Temp.GetInteger("State") != 1)
            {
                Temp.SetInteger("State", 1);
            }
            gameObject.transform.localScale = new Vector3(-1, 1, 1);
        }else if(gameObject.transform.position.x < -.75f)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = States[0];
            if (Temp.GetInteger("State") != 1)
            {
                Temp.SetInteger("State", 1);
            }
            gameObject.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            if(gameObject.transform.position.y > .1f)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = States[1];
                if (Temp.GetInteger("State") != 2)
                {
                    Temp.SetInteger("State", 2);
                }
            }
            else if(gameObject.transform.position.y < -.1f)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = States[2];
                if (Temp.GetInteger("State") != 3)
                {
                    Temp.SetInteger("State", 3);
                }
            }
        }
    }
    IEnumerator EffectStun(float _StunTime)
    {
        
        yield return new WaitForSeconds(.18f);
       
        WaveHit = false;
        CenterHit = false;
        StartCoroutine(TimeStun(_StunTime));
    }
    IEnumerator TimeStun(float _StunTime)
    {
        yield return new WaitForSeconds(_StunTime);
        TimerStarted = false;
        Stunned = false;
    }
    public void Activate()
    {
        StartCoroutine(EnemyMovement());
        StartCoroutine(DistanceCheck());
        StartCoroutine(DeathCheck());
    }
}
