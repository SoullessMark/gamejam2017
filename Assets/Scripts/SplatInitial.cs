﻿using UnityEngine;
using System.Collections;

public class SplatInitial : MonoBehaviour {
    public Sprite[] Splats;
    
	// Use this for initialization
	void Start () {
        gameObject.GetComponent<SpriteRenderer>().sprite = Splats[Random.Range(0, 4)];
        ScaleCheck();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void ScaleCheck()
    {
        gameObject.transform.localScale = new Vector3(Random.Range(.8f, 1.25f), Random.Range(.25f, .65f));
    }
}
