﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CenterBehaviour : MonoBehaviour {
    public Image HealthBar;
    public float MaxHealth = 100;
    public float Health = 100;
    public float PercentHealth;
    public float TargetPercentHealth;
    private bool HasHit;
    private bool running = true;
	// Use this for initialization
	void Start () {
        PercentHealth = 1;
        HealthBar.fillAmount = 1;
        TargetPercentHealth = 1;
        StartCoroutine(UpdateHealth());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Enemy" && !HasHit)
        {
            HasHit = true;
            col.gameObject.GetComponent<EnemyBehaviour>().CenterHit = true;
            col.gameObject.GetComponent<EnemyBehaviour>().Stunned = true;
            Health -= 10;
            TargetPercentHealth = (Health / MaxHealth);
            HasHit = false;
            StartCoroutine(ClearCenterHit(col.gameObject.GetComponent<EnemyBehaviour>()));
        }
    }
    IEnumerator UpdateHealth()
    {
        while (running)
        {
            if(PercentHealth > TargetPercentHealth + .001f)
            {
                PercentHealth = Mathf.Lerp(PercentHealth, TargetPercentHealth, Time.deltaTime * 30);
                HealthBar.fillAmount = PercentHealth;
            }
            yield return new WaitForSeconds(.05f);
        }
    }
   
    IEnumerator ClearCenterHit(EnemyBehaviour col)
    {
        yield return new WaitForSeconds(.2f);
        col.CenterHit = false;

    }
}
