﻿using UnityEngine;
using System.Collections;

public class SplatParticle : MonoBehaviour {
    public Animator Particle;
    public bool active = true;
    public bool BlowingUp = true;
    public Vector3 Target;
	// Use this for initialization
	void Start () {
        Particle = gameObject.transform.GetComponentInChildren<Animator>();
        StartCoroutine(ParticleMove());
        StartCoroutine(Splat());
        
	}
	public void _Splat()
    {
        StartCoroutine(Splat());
        StartCoroutine(ParticleMove());
    }
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator ParticleMove()
    {
        while (active)
        {
            if (BlowingUp)
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, Target, Time.deltaTime * 65f);
            }
            yield return new WaitForSeconds(.02f);
        }
       
    }
    IEnumerator Splat()
    {
        while (active)
        {
            StartCoroutine(BlowUpReset());
            Particle = gameObject.transform.GetComponentInChildren<Animator>();
            Particle.SetInteger("State", 1);
            
            yield return new WaitForSeconds(GameLogicUtil.GameLogicUtil.Splat.length - .01f);
            SplatPooler TempPool = GameLogicUtil.GameLogicUtil.Pooler;
            for(int i = 0; i < TempPool.MaxStaticSplats; i++)
            {
                if(TempPool.StaticSplats[i].activeInHierarchy == false)
                {
                    TempPool.StaticSplats[i].transform.position = gameObject.transform.position;
                    Vector3 TempScale = TempPool.StaticSplats[i].transform.localScale;
                   
                    TempPool.StaticSplats[i].SetActive(true);
                    TempPool.StaticSplats[i].transform.localScale = new Vector3(TempScale.x / 10.5f, TempScale.y / 10.5f, 1f);
                    break;
                }
            }
            Particle.SetInteger("State", 0);
            active = false;
            StartCoroutine(SplatReset());
            
        }
    }
    IEnumerator SplatReset()
    {
        yield return new WaitForSeconds(.1f);
        active = true;
        BlowingUp = false;
        gameObject.SetActive(false);
    }
    IEnumerator BlowUpReset()
    {
        BlowingUp = true;
        yield return new WaitForSeconds(.15f);
        BlowingUp = false;
    }
    
}
