﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
    private bool running = true;
    public int MAX_ENEMIES;
    public GameObject Enemy;
    public GameObject[] Enemies;
	// Use this for initialization
	void Start () {
        Enemies = new GameObject[MAX_ENEMIES];
        for(int i = 0; i < MAX_ENEMIES; i++)
        {
            Enemies[i] = (GameObject) Instantiate(Enemy, gameObject.transform.position,gameObject.transform.rotation);
            Enemies[i].SetActive(false);
        }
        StartCoroutine(SpawnEnemy());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator SpawnEnemy()
    {
        while (running)
        {
            for(int i = 0;i < MAX_ENEMIES; i++)
            {
                if(Enemies[i].gameObject.activeInHierarchy == false)
                {
                    SpawnPlace(Enemies[i]);
                    Enemies[i].SetActive(true);
                    EnemyBehaviour temp = Enemies[i].gameObject.GetComponent<EnemyBehaviour>();
                    temp.Activate();
                }
                yield return new WaitForSeconds(1.5f);
            }
            
        }
    }
    void SpawnPlace(GameObject enemy)
    {
        int RandPos = Random.Range(1, 5);
        if(RandPos == 1)
        {
            enemy.transform.position = new Vector3(Random.Range(-10.25f, 10.25f), 6.5f, 0);
        }else if(RandPos == 2)
        {
            enemy.transform.position = new Vector3(Random.Range(-10.25f, 10.25f), -6.5f, 0);
        }else if(RandPos == 3)
        {
            enemy.transform.position = new Vector3(-10.25f, Random.Range(-6.5f, 6.5f), 0);
        }else if(RandPos == 4)
        {
            enemy.transform.position = new Vector3(10.25f, Random.Range(-6.5f, 6.5f), 0);
        }
    }
}
