﻿using UnityEngine;
using System.Collections;

public class EnemyDetection : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Enemy")
        {

            StartCoroutine(ClearEnemyStats(col.gameObject.GetComponent<EnemyBehaviour>()));

            if (GameLogicUtil.GameLogicUtil.HandAnim.GetInteger("State") == 1)
            {
               
                col.gameObject.GetComponent<EnemyBehaviour>().WaveHit = true;
                col.gameObject.GetComponent<EnemyBehaviour>().Stunned = true;
            }else if(GameLogicUtil.GameLogicUtil.HandAnim.GetInteger("State") == 2)
            {
                col.gameObject.GetComponent<EnemyBehaviour>().Dead = true;
            }
        }
    }

    IEnumerator ClearEnemyStats(EnemyBehaviour col)
    {
        yield return new WaitForSeconds(.2f);
        col.WaveHit = false;
        
        col.CenterHit = false;

        StartCoroutine(ClearEnemyStuns(col));
    }
   
    IEnumerator ClearEnemyStuns(EnemyBehaviour col)
    {
        yield return new WaitForSeconds(10.25f);

        col.Stunned = false;
    }
}
